const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: '',
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html'
    },
    redirect: {
      entry: 'src/redirect.js',
      template: 'public/redirect.html',
      filename: 'redirect.html'
    }
  }
})
